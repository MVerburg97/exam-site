#!/bin/bash

#while loop die door de lines van data.txt gaat
while IFS= read -r line
do
#person[ geeft aan dat er een nieuwe persoon komt dus vanaf hier wordt de data elke keer gelezen
  if [[ $line == "person[" ]]; then
    read -r linewithname
    read -r linewithemail
    read -r linewithcode
#https://www.cyberciti.biz/faq/unix-howto-read-line-by-line-from-file/

    name=$(echo "$linewithname" | awk -F'"' '{print $2}')
    email=$(echo "$linewithemail" | awk -F'"' '{print $2}')
    code=$(echo "$linewithcode" | awk -F '"' '{print $2}')

#    https://stackoverflow.com/questions/16040567/use-awk-to-extract-substring
     json="{\"email\":\"$email\",\"shortname\":\"$code\",\"fullname\":\"$name\"}"

#     https://unix.stackexchange.com/questions/30903/how-to-escape-quotes-in-shell

      curl -d "$json" -H 'Content-Type: application/json' http://localhost:5000/

  fi

done < data.txt

# The json format in which your need to post the data has to look like this:
# { 
#     "email": "example@example.com,
#     "shortname": "abc01",
#     "fullname": "name of person"
# }
